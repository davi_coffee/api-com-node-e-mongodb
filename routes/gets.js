const express = require("express");
const router = express.Router();
const path = require("path");

const User = require("../models/users");

router.get("/", (_req, res) => {
  return res
    .status(200)
    .sendFile(path.join(__dirname + "/static/home/index.html"));
});

router.get("/logout", (req, res) => {
  const token = req.cookies.auth;

  User.findByToken(token, (err, user) => {
    if (err) {
      return res.status(400).json(err);
    }
    if (!user) {
      return res.status(400).json({
        message: "Usuário não está logado",
      });
    }

    User.findOneAndUpdate({ _id: user._id }, { token: "" }, (err, _user) => {
      if (err) {
        return res.status(400).json({
          message: "Erro ao deslogar",
        });
      }
      return res.clearCookie("auth").sendStatus(200);
    });
  });
});

router.get("/profile", (req, res) => {
  const token = req.cookies.auth;

  User.findByToken(token, (err, user) => {
    if (err) {
      return res.status(400).json(err);
    }
    if (!user) {
      return res.status(400).json({
        message: "Usuário não está logado",
      });
    }

    return res.status(200).json(user);
  });
});

router.get("/verify", (req, res) => {
  const tokenVerifyEmail = req.query.id;
  User.findByTokenVerifyEmail(tokenVerifyEmail, (err, user) => {
    if (err) {
      return res
        .status(400)
        .sendFile(path.join(__dirname + "/static/verify-email/invalid.html"));
    }

    User.findOneAndUpdate(
      { _id: user._id },
      { is_auth: true, token_verify: "" },
      (err, _user) => {
        if (err) {
          return res
            .status(400)
            .json({ err })
            .sendFile(
              path.join(__dirname + "/static/verify-email/invalid.html")
            );
        }
        return res
          .status(200)
          .sendFile(path.join(__dirname + "/static/verify-email/index.html"));
      }
    );
  });
});

router.get("/recovery", (req, res) => {
  const tokenVerifyEmail = req.query.id;
  User.findByTokenVerifyEmail(tokenVerifyEmail, (err, _user) => {
    if (err) {
      return res
        .status(400)
        .sendFile(path.join(__dirname + "/static/verify-email/invalid.html"));
    }
    return res
      .status(200)
      .sendFile(path.join(__dirname + "/static/recovery-password/form.html"));
  });
});

module.exports = router;
