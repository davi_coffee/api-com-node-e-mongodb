const express = require("express");
const router = express.Router();
const sendEmail = require("../email/mail");
const User = require("../models/users");
const path = require("path");

router.post("/register", (req, res) => {
  const newUser = new User(req.body);

  if (newUser.password !== newUser.password_confirm) {
    return res.status(400).json({
      message: "As senhas não são correspondente.",
    });
  }

  User.findOne({ email: newUser.email }, (err, user) => {
    if (user) {
      return res.status(400).json({
        message: "Esse email já está cadastrado.",
      });
    }
    newUser.save((err, doc) => {
      if (err) {
        return res.status(400).json({
          error: err,
          message: "Falha no cadastramento",
        });
      }
      const link = `http://${req.get("host")}/verify?id=${doc.token_verify}`;

      sendEmail(
        newUser.email,
        "Obrigado por se cadastrar!",
        `<h2>Olá, tudo bem?</h2><br><p>Por favor, clique no link para verificar seu email.</p><a href="${link}">Clique aqui!</a>`
      )
        .then((_res) => {
          return res.status(200).json({
            message: "OK",
            user: doc,
          });
        })
        .catch((err) => {
          return res.status(500).json({
            error: err,
          });
        });
    });
  });
});

router.post("/login", (req, res, next) => {
  const userLogin = req.body;
  const token = req.cookies.auth;

  User.findByToken(token, (err, user) => {
    if (err) {
      return res.status(400).json(err);
    }
    if (user) {
      return res.status(400).json({
        isAuth: true,
        message: "Usuário já está logado",
      });
    }
    User.findOne({ email: userLogin.email }, (err, user) => {
      if (err) {
        return res.status(400).json({
          isAuth: false,
          error: err,
        });
      }
      if (!user) {
        return res.status(400).json({
          isAuth: false,
          message: "Falha, este email não está cadastrado",
        });
      }
      user.comparePassword(userLogin.password, (err, isMatch) => {
        if (err) {
          return res.status(400).json({
            message: "Erro com a senha",
            error: err,
          });
        }
        if (!isMatch) {
          return res.status(400).json({
            isAuth: false,
            message: "Senha inválida",
          });
        }
        user.generateToken((err, user) => {
          if (err) {
            return res.status(400).json({
              message: "Erro com o token",
            });
          }
          res.cookie("auth", user.token).json(user);
        });
      });
    });
  });
});

router.post("/send-token", (req, res) => {
  const { email } = req.body;
  User.findOne({ email }, null, (err, user) => {
    if (err) {
      return res.status(400).json({
        isAuth: false,
        error: err,
      });
    }
    if (!user) {
      return res.status(400).json({
        isAuth: false,
        message: "Falha, este email não está cadastrado",
      });
    }
    user.generateTokenVerify((err, user) => {
      if (err) {
        return res.send(500).json({ error: err });
      }
      const link = `http://${req.get("host")}/recovery?id=${user.token_verify}`;

      sendEmail(
        email,
        "Recuperação de Senha",
        `Ola,<br> Por favor, clique no link para modificar a senha.<br><a href="${link}">Clique aqui</a>`
      )
        .then((_res) => {
          return res.status(200).json({
            message: "OK",
          });
        })
        .catch((err) => {
          return res.status(400).json({
            err,
          });
        });
    });
  });
});

router.post("/recovery", (req, res) => {
  tokenVerifyUrl = req.query.id;
  const { password, password_confirm } = req.body;

  if (password !== password_confirm) {
    return res
      .status(400)
      .sendFile(
        path.join(__dirname + "/static/recovery-password/invalid.html")
      );
  }
  User.findByTokenVerifyEmail(tokenVerifyUrl, (err, user) => {
    if (err) {
      return res
        .status(400)
        .sendFile(
          path.join(__dirname + "/static/recovery-password/invalid.html")
        );
    }
    user.password = password;
    user.password_confirm = password_confirm;
    user.save((err, doc) => {
      if (err) {
        return res.sendStatus(500);
      }
      const link = `http://${req.get("host")}/verify?id=${doc.token_verify}`;

      sendEmail(
        user.email,
        "Sua senha foi modificada!",
        `<h2>Olá, tudo bem?</h2><br><p>Por favor, clique no link para confirma a mudança de senha.</p><a href="${link}">Clique aqui!</a>`
      )
        .then((_res) => {
          return res
            .status(200)
            .sendFile(
              path.join(__dirname + "/static/recovery-password/valid.html")
            );
        })
        .catch((err) => {
          return res.status(500).json({
            error: err,
          });
        });
    });
  });
});

module.exports = router;
