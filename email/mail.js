const nodemailer = require("nodemailer");

module.exports = (to, subject, html) => {
  const smtpTransport = nodemailer.createTransport({
    host: process.env.SMTP_SERVER,
    port: parseInt(process.env.SMTP_PORT),
    secure: false,
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD,
    },
  });

  smtpTransport.verify((err, _sucess) => {
    if (err) {
      return err;
    }
  });

  const message = {
    from: process.env.SMTP_USERNAME,
    to,
    subject,
    html,
  };

  return new Promise((resolve, reject) => {
    smtpTransport
      .sendMail(message)
      .then((res) => {
        smtpTransport.close();
        return resolve(res);
      })
      .catch((err) => {
        smtpTransport.close();
        return reject(err);
      });
  });
};
