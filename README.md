# API Cadastro com Node.js + MongoDb

**Este é um back-end para uma pequena aplicação pessoal. O objetivo desse aplicação é criar uma API com endpoints para registro, login, logout, profile e com recursos de recuperação de senha e confirmação de email.**

## Tecnologias

Essa aplicação foi criada com auxílio de variáveis de ambientes, por questão de segurança, e com as seguintes tecnologias:

- Node.js
- MongoDb Atlas (Mongoose)
- Express.js
- JWT
- Bcrypt
- Cookies
- Nodemailer
- Bootstrap

## **EndPoints**

Foram criadas nove endpoints para essa API, sendo que as rotas logout, profile, recovery e verify o usuário deve está autenticado com os tokens de acesso.

### Register

`POST /register - FORMATO DA REQUISIÇÃO`

```
{
	"name": "Fulano de Silva",
	"email":"fulano@gmail.com",
	"password": "123456",
	"password_confirm": "123456",
    "access_level": "admin"
}
```

Caso tudo de certa na requisição a resposta será:

`POST /register - FORMATO DA RESPOSTA - STATUS 200`

```
{
  "message": "OK",
  "user": {
    "is_auth": false,
    "_id": "604de3158915c43f0bba17a9",
    "name": "Fulano de Silva",
    "email": "fulano@gmail.com",
    "password": "$2b$10$6.j1mA1sSi/FRRMlTtEoIOpjLdb9xh9E4am5glIQfeRZRNlkERD/e",
    "password_confirm": "$2b$10$6.j1mA1sSi/FRRMlTtEoIOpjLdb9xh9E4am5glIQfeRZRNlkERD/e",
    "access_level": "admin",
    "token_verify": "eyJhbGciOiJIUzI1NiJ9.ZGF2aS5hbmRyYWRlLjQyMDFAZ21haWwuY29t.m0KIvfRL6811qwaA5AAPq1cufDbH6qvwgrVMW0hpKZU",
    "__v": 0
  }
}
```

**Especificações do campos:**

1. name
    - Obrigatório
    - String

2. email
    - Obrigatório
    - Único
    - String
    - Não pode conter espaço

3. password
    - Obrigatório
    - String
    - Mínimo 6 caracteres

4. password_confirm
    - Obrigatório
    - String
    - Mínimo 6 caracteres
    - Deve ser igual ao password

5. access_level
    - String
    - Caso não colocado, o valor será "user"

### Login

`POST /login - FORMATO DA REQUISIÇÃO`

```
{
	"email":"fulano@gmail.com",
	"password": "123456"
}
```

Caso o login dê certo, a resposta será:

`POST /login - FORMATO DA RESPOSTA - STATUS 200`

```
{
  "is_auth": false,
  "_id": "604de3158915c43f0bba17a9",
  "name": "Fulano da Silva",
  "email": "fulano@gmail.com",
  "password": "$2b$10$b3W7ZLw.GV.XAAp369hlSuWxLltMj90r6u8yMKN7b4n5KimevsktG",
  "password_confirm": "$2b$10$b3W7ZLw.GV.XAAp369hlSuWxLltMj90r6u8yMKN7b4n5KimevsktG",
  "access_level": "admin",
  "token_verify": "",
  "__v": 0,
  "token": "eyJhbGciOiJIUzI1NiJ9.NjA0ZGUzMTU4OTE1YzQzZjBiYmExN2E5.Kszs18GWqjX8rqbKEPnczrP6r_dpjfcab695Dv7rU-Q"
}
```

### Profile

`GET /profile - FORMATO DA RESPOSTA - STATUS 200`

```
{
  "is_auth": false,
  "_id": "604de3158915c43f0bba17a9",
  "name": "Fulano da Silva",
  "email": "fulano@gmail.com",
  "password": "$2b$10$b3W7ZLw.GV.XAAp369hlSuWxLltMj90r6u8yMKN7b4n5KimevsktG",
  "password_confirm": "$2b$10$b3W7ZLw.GV.XAAp369hlSuWxLltMj90r6u8yMKN7b4n5KimevsktG",
  "access_level": "admin",
  "token_verify": "",
  "__v": 0,
  "token": "eyJhbGciOiJIUzI1NiJ9.NjA0ZGUzMTU4OTE1YzQzZjBiYmExN2E5.Kszs18GWqjX8rqbKEPnczrP6r_dpjfcab695Dv7rU-Q"
}
```

### Logout

`GET /logout - FORMATO DA RESPOSTA - STATUS 200`

```
OK
```

### Confirmar Senha
> O usuário irá receber um email com o link para confirma seu email e clicando no link, será redirecionado para uma página e dessa forma, is_auth será true, confirmando que o email está confirmado.

![Texto alternativo](./imagens/verify_email.png)

`GET /revery?id=token_verify - FORMATO DA RESPOSTA - STATUS 200`

![Texto alternativo](./imagens/get_page_verify.png)



## Recuperar senha
### Pedir nova senha
> O usuário deve informa uma email registrado no banco de dados e irá receber um link para uma página que terá um formulário para a nova senha.

`POST /send-token - FORMATO DA REQUISIÇÃO`

```
{
	"email":"fulano@gmail.com"
}
```

Caso o pedido de nova senha dê certo, a resposta será:

`POST /send-token - FORMATO DA RESPOSTA - STATUS 200`

```
{
  "message": "OK"
}
```

### Escrever nova senha
> Quando o usuário requisitar uma nova senha, irá receber um email que terá o link para o formulário de nova senha.

![Texto alternativo](./imagens/get_modify_password.png)

`GET /recovery?id=token_verify - FORMATO DA RESPOSTA - STATUS 200`

![Texto alternativo](./imagens/get_page_recovery.png)


### Enviar formulário para nova senha
> Apos o preenchimento do furmulário na pagina "/recovery?id=", ele irá enviar no action do furmlário o seguinte corpo:

`POST /recovery?id=token_verify - FORMATO DA REQUISIÇÃO DO ACTION FORMULÁRIO`

```
{
	"password": "123456",
	"password_confirm": "123456"
}
```

Caso o formulário dê certo, ele terá seguinte resposta:

`POST /recovery?id=token_verify - FORMATO DA RESPOSTA - STATUS 200`

![Texto alternativo](./imagens/post_recovery.png)



## Erros

A api é detalhada em relação aos erros que podem ocorrer, ou seja, sempre virá com uma menssagem explicando os erros cometidos e até mesmo erros do Banco. Existe também console.log que explicam o erro caso venha acontecer, dessa forma será possível descobrir rápido onde está o erro e consertar.

## Conclusão

Api é ideal para registros e autenticação simples de usuário, como um controle de um site. Foi trabalhado com Queries simples do mongoose e sempre trantando os erros possiveis,pode ser modificado o host das paginas ou seu visual, conforme for necessário.

### TaskList

- [x] Construir Servidor e suas dependências
- [x] Conectar ao banco de dados MongoDB Atlas
- [x] Modelagem do banco de dados com Mongoose
- [x] Inserir rota register
- [x] Inserir autenticação com token
- [x] Inserir rota login
- [x] Inserir rota logout
- [x] Inserir rota profile
- [x] Adicionar verificação de email
- [x] Adicionar recuperar senha com email
- [x] Inserir pagina de verificação email
- [x] Inserir pagina de recuperação senha
- [x] Refatoração da aplicação
- [x] Reescrever o README
- [ ] Criar testes TDD

