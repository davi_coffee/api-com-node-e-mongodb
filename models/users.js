const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const config = require("../config/config").get("production");

const salt = 10;

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
    unique: 1,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  },
  password_confirm: {
    type: String,
    required: true,
    minlength: 6,
  },
  access_level: {
    type: String,
    required: false,
  },
  is_auth: {
    type: Boolean,
    default: false,
  },
  token_verify: {
    type: String,
  },
  token: {
    type: String,
  },
});

userSchema.pre("save", function (next) {
  const user = this;
  const token_verify = jwt.sign(user.email, config.SECRET);

  if (!user.access_level) {
    user.access_level = "user";
  }

  if (user.isModified("password")) {
    bcrypt.genSalt(salt, (err, salt) => {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) {
          return next(err);
        }
        user.token_verify = token_verify;
        user.password = hash;
        user.password_confirm = hash;
        next();
      });
    });
  } else {
    next();
  }
});

// comparar senhas
userSchema.methods.comparePassword = function (password, callback) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    if (err) {
      return callback(err);
    }
    callback(null, isMatch);
  });
};

// gerar token
userSchema.methods.generateToken = function (callback) {
  const user = this;
  const token = jwt.sign(user._id.toHexString(), config.SECRET);

  user.token = token;

  user.save((err, user) => {
    if (err) {
      return callback(err);
    }
    return callback(null, user);
  });
};

userSchema.statics.findByToken = function (token, callback) {
  const user = this;

  jwt.verify(token, config.SECRET, (_err, decode) => {
    user.findOne({ _id: decode, token: token }, function (err, user) {
      if (err) {
        return callback({ error: "Token inválido" }, undefined);
      }
      return callback(undefined, user);
    });
  });
};

userSchema.statics.findByTokenVerifyEmail = function (
  tokenVerifyEmail,
  callback
) {
  const user = this;

  jwt.verify(tokenVerifyEmail, config.SECRET, (_err, decode) => {
    user.findOne(
      { email: decode, token_verify: tokenVerifyEmail },
      function (err, user) {
        if (err) {
          return callback(err, undefined);
        }

        if (!user) {
          return callback({ error: "Usuario não encontrado" }, undefined);
        }
        callback(undefined, user);
      }
    );
  });
};

userSchema.methods.generateTokenVerify = function (callback) {
  const user = this;
  const token_verify = jwt.sign(user.email, config.SECRET);

  user.token_verify = token_verify;
  user.save((err, user) => {
    if (err) {
      return callback(err);
    }
    return callback(null, user);
  });
};

userSchema.methods.saveNewPassword = function (password, callback) {
  bcrypt.genSalt(salt, (err, salt) => {
    if (err) {
      return callback(err);
    }
    bcrypt.hash(password, salt, (err, hash) => {
      if (err) {
        return callback(err);
      }
      return callback(null, hash);
    });
  });
};
module.exports = mongoose.model("User", userSchema);
