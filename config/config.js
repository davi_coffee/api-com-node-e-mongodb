require("dotenv/config")

const config = {
    production: {
        SECRET: process.env.SECRET,
        DATABASE: process.env.MONGODB_ATLAS,
    },
    default: {
        SECRET: "mysecret",
        DATABASE: "mongo.exe mongodb://$[hostlist]/$[database]?authSource=$[authSource] --username $[username]"
    },
}


exports.get = (env) => {
    return config[env] || config.default
}
