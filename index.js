require("dotenv/config");
const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

// Conexão com banco de dados
const mongodb = require("./config/config").get("production");

mongoose.Promise = global.Promise;
mongoose.connect(
  mongodb.DATABASE,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
  (err) => {
    if (err) {
      console.log(err);
      console.log("Erro ao conectar ao banco de dados");
    } else {
      console.log("Conectado ao banco de dados");
    }
  }
);

// Rotas
const routerGets = require("./routes/gets");
const routerPosts = require("./routes/posts");

app.use("/", routerGets);
app.use("/", routerPosts);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server - http://localhost:${PORT}/`);
});
